==========
Quickstart
==========

A friendly message about how easy it is to use pop-create

Getting Started
===============

Start by installing pop-create and making a new directory for your project:

.. code-block:: bash

    $ pip3 install pop-create
