.. pop-create documentation master file, created by
   sphinx-quickstart on Mon Jun 25 20:19:13 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pop-create's Documentation!
======================================


.. toctree::
   :maxdepth: 2
   :glob:

   tutorial/quickstart
   topics/pop_create
   releases/*


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
