import tempfile


def test_cli(mock_hub, hub):
    mock_hub.pop_create.init.cli = hub.pop_create.init.cli
    mock_hub.pop_create.init.cli()

    mock_hub.pop.config.load.assert_called_once_with(["pop_create"], "pop_create")
