import os
import pathlib
import tempfile
from unittest import mock

from tests.integration.conftest import compare_dir_trees


def test_cli(hub):
    with tempfile.TemporaryDirectory(prefix="test_", suffix="_pop_create") as temp_dir:
        temp_path = pathlib.Path(temp_dir)
        # Verify that the temporary directory is completely empty
        assert not os.listdir(temp_dir)

        # Run the pop-create cli with mocked arguments
        with mock.patch(
            "sys.argv",
            [
                "pop-create",
                "docs",
                "--overwrite-existing",
                f"--directory={temp_dir}",
                "--project-name=test-project",
            ],
        ):
            hub.pop_create.init.cli()

        compare_dir_trees(
            os.walk(temp_dir),
            [
                (temp_dir, ["docs", "requirements"], []),
                (
                    str(temp_path / "docs"),
                    ["tutorial", "topics", "releases"],
                    ["Makefile", "make.bat", "index.rst", "conf.py", "sitevars.rst"],
                ),
                (str(temp_path / "requirements"), [], ["docs.txt"]),
                (
                    str(temp_path / "docs" / "releases"),
                    [],
                    ["0.1.0.rst", "index.rst"],
                ),
                (
                    str(temp_path / "docs" / "topics"),
                    [],
                    ["test-project.rst", "contributing.rst", "license.rst"],
                ),
                (
                    str(temp_path / "docs" / "tutorial"),
                    [],
                    ["example.rst", "index.rst"],
                ),
            ],
        )
