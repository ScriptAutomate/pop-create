import os
import pathlib
import tempfile
from unittest import mock

from tests.integration.conftest import compare_dir_trees


def test_cli(hub):
    with tempfile.TemporaryDirectory(prefix="test_", suffix="_pop_create") as temp_dir:
        temp_path = pathlib.Path(temp_dir)
        # Verify that the temporary directory is completely empty
        assert not os.listdir(temp_dir)

        # Run the pop-create cli with mocked arguments
        with mock.patch(
            "sys.argv",
            [
                "pop-create",
                "tests",
                "--overwrite-existing",
                f"--directory={temp_dir}",
                "--project-name=test-project",
            ],
        ):
            hub.pop_create.init.cli()

        compare_dir_trees(
            os.walk(temp_dir),
            [
                (temp_dir, ["__pycache__", "tests", "requirements"], ["pytest.ini"]),
                (
                    str(temp_path / "__pycache__"),
                    [],
                    [],
                ),
                (str(temp_path / "requirements"), [], ["tests.in"]),
                (
                    str(temp_path / "tests"),
                    ["integration", "unit"],
                    ["__init__.py"],
                ),
                (
                    str(temp_path / "tests" / "integration"),
                    ["test_project"],
                    ["__init__.py", "conftest.py"],
                ),
                (
                    str(temp_path / "tests" / "integration" / "test_project"),
                    [],
                    ["__init__.py", "test_init.py"],
                ),
                (
                    str(temp_path / "tests" / "unit"),
                    ["test_project"],
                    ["__init__.py", "conftest.py"],
                ),
                (
                    str(temp_path / "tests" / "unit" / "test_project"),
                    [],
                    ["__init__.py", "test_init.py"],
                ),
            ],
        )


def test_vertical(hub):
    with tempfile.TemporaryDirectory(prefix="test_", suffix="_pop_create") as temp_dir:
        temp_path = pathlib.Path(temp_dir)
        # Verify that the temporary directory is completely empty
        assert not os.listdir(temp_dir)

        # Run the pop-create cli with mocked arguments
        with mock.patch(
            "sys.argv",
            [
                "pop-create",
                "tests",
                "--overwrite-existing",
                f"--directory={temp_dir}",
                "--vertical",
                "--project-name=test-project",
            ],
        ):
            hub.pop_create.init.cli()

        compare_dir_trees(
            os.walk(temp_dir),
            [
                (temp_dir, ["tests", "requirements"], ["pytest.ini"]),
                (str(temp_path / "requirements"), [], ["tests.in"]),
                (
                    str(temp_path / "tests"),
                    ["integration", "unit"],
                    ["__init__.py"],
                ),
                (
                    str(temp_path / "tests" / "integration"),
                    [],
                    ["__init__.py", "conftest.py"],
                ),
                (
                    str(temp_path / "tests" / "unit"),
                    [],
                    ["__init__.py", "conftest.py"],
                ),
            ],
        )
