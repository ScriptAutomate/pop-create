import os
import pathlib
import tempfile
from unittest import mock

from tests.integration.conftest import compare_dir_trees


def test_cli(hub):
    with tempfile.TemporaryDirectory(prefix="test_", suffix="_pop_create") as temp_dir:
        temp_path = pathlib.Path(temp_dir)
        # Verify that the temporary directory is completely empty
        assert not os.listdir(temp_dir)

        # Run the pop-create cli with mocked arguments
        with mock.patch(
            "sys.argv",
            [
                "pop-create",
                "--vertical",
                "--overwrite-existing",
                f"--directory={temp_dir}",
                "--project-name=test-project",
            ],
        ):
            hub.pop_create.init.cli()

        compare_dir_trees(
            os.walk(temp_dir),
            [
                (
                    temp_dir,
                    [
                        "__pycache__",
                        "tests",
                        "docs",
                        "requirements",
                        "test_project",
                    ],
                    [
                        ".coveragerc",
                        ".gitlab-ci.yml",
                        ".gitignore",
                        ".pre-commit-config.yaml",
                        ".pyproject.toml",
                        "build.conf",
                        "noxfile.py",
                        "pytest.ini",
                        "setup.py",
                        "LICENSE",
                        "README.rst",
                        "CONTRIBUTING.rst",
                    ],
                ),
                (
                    str(temp_path / "__pycache__"),
                    [],
                    [],
                ),
                (
                    str(temp_path / "docs"),
                    ["tutorial", "topics", "releases"],
                    ["Makefile", "make.bat", "index.rst", "conf.py", "sitevars.rst"],
                ),
                (
                    str(temp_path / "docs" / "releases"),
                    [],
                    ["0.1.0.rst", "index.rst"],
                ),
                (
                    str(temp_path / "docs" / "topics"),
                    [],
                    ["test-project.rst", "contributing.rst", "license.rst"],
                ),
                (
                    str(temp_path / "docs" / "tutorial"),
                    [],
                    ["example.rst", "index.rst"],
                ),
                (
                    str(temp_path / "requirements"),
                    ["py3.10", "py3.6", "py3.7", "py3.8", "py3.9"],
                    ["tests.in", "base.txt", "docs.txt"],
                ),
                (str(temp_path / "requirements" / "py3.10"), [], ["tests.txt"]),
                (str(temp_path / "requirements" / "py3.6"), [], ["tests.txt"]),
                (str(temp_path / "requirements" / "py3.7"), [], ["tests.txt"]),
                (str(temp_path / "requirements" / "py3.8"), [], ["tests.txt"]),
                (str(temp_path / "requirements" / "py3.9"), [], ["tests.txt"]),
                (
                    str(temp_path / "test_project"),
                    [],
                    ["version.py", "conf.py"],
                ),
                (
                    str(temp_path / "tests"),
                    ["integration", "unit"],
                    ["__init__.py"],
                ),
                (
                    str(temp_path / "tests" / "integration"),
                    [],
                    ["__init__.py", "conftest.py"],
                ),
                (
                    str(temp_path / "tests" / "unit"),
                    [],
                    ["__init__.py", "conftest.py"],
                ),
            ],
        )
