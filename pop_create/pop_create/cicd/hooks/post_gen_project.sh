#!/bin/bash

if command -v git
then
  git init || echo "Could not initialize git"
  git add .|| echo "Could not add files"
  if command -v pre-commit
  then
    pre-commit install || echo "pre-commit is not installed"
    pre-commit run -av || echo $?
  fi
  # Remove git from the temporary directory
  rm -rf .git
fi


if command -v tree
then
  tree .
fi
